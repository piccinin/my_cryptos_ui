
$("#sair").click( function () {

  google.accounts.id.revoke('piccininsouza@gmail.com', done => {
    if (done.error){
      alert("Usuário não está logado");
    }
    if (done.successful){
      alert("Usuário deslogado com sucesso");
      window.location.href = "/login";
    }
    console.log(done);
  });

});

function googleLoginHandle(response){

  $.ajax({
    type: 'POST',
    url: 'http://localhost:5000/login',
    data: JSON.stringify ({credential: response.credential}),
    contentType: "application/json",
    dataType: 'json'
  }).done(function(response){

    console.log('Você logou');
    document.cookie = "token="+response.auth_token;
    document.cookie = "email="+response.email;
    console.log(response);
    window.location.href = "/";

  }).fail(function(jqXHR, textStatus){
    //Se o usuário não estiver
    if ( jqXHR.status === 500 ){
      google.accounts.id.revoke(jqXHR.responseJSON.email, done => {
        if (done.error){
          console.log("Usuário não está logado");
        }
        if (done.successful){
          console.log("Usuário deslogado com sucesso");
        }
      });
      alert("Erro do servidor!");
    }

    if ( jqXHR.status === 401 ){
      google.accounts.id.revoke(jqXHR.responseJSON.email, done => {
        if (done.error){
          console.log("Usuário não está logado");
        }
        if (done.successful){
          console.log("Usuário deslogado com sucesso");
        }
      });
      alert("Usuário não cadastrado");
    }
    window.location.href = "/login";

  });
}



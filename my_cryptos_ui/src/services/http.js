import axios from 'axios';

export default function (app) {

  const userSession =  app.$cookies.get('user_session');

  const axiosInstance = axios.create({
    baseURL: app.$store.state.baseUrl,
    headers: {
      'x-access-token': userSession
    }
  });

  return axiosInstance;
};
import {createStore} from 'vuex';

export default createStore({
  state:{
    userData: null,
    usuario: '',
    senha:'',
    baseUrl: 'https://my_cryptos.devfpl.com.br'
  },
  getters: {},
  modules: {}
});
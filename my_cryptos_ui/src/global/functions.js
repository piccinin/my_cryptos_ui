
 const globalFunctions = (vueApp) => {
  //Metodo global para formatar a exibicao do valor das moedas
  vueApp.mixin({
    methods: {
      formatCurrencies(currencySymbol, value){
        return new Intl.NumberFormat('pt-BR', { style: 'currency', currency: currencySymbol }).format(value);
      },
      formatMarketCap(mcap){
        const multiplier = [' Mi', ' Bi', ' Tri'];
        const divisor = [1000000, 1000000000, 1000000000000];
        for (let x = 2; x >=0; x--){
          if ( (mcap / divisor[x]) > 1 ){
            return this.formatCurrencies('USD', (mcap / divisor[x])) + multiplier[x];
          }
        }
        return '';
      }
    }
  });
}

export default globalFunctions;
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import globalFunctions from './global/functions'
import VueCookies from 'vue-cookies'
import vue3GoogleLogin from 'vue3-google-login'


require('./assets/css/my_styles.css');

const vueApp = createApp(App);

globalFunctions(vueApp);

vueApp.use(vue3GoogleLogin, {
  clientId: '747002520458-h4234tjq3mqf5gf887lhernom8q7j3bu.apps.googleusercontent.com'
});
vueApp.use(store).use(router).mount('#app')
vueApp.use(VueCookies);
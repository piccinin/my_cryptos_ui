import { createRouter, createWebHistory } from 'vue-router';
import Home from '../views/Home.vue';
import Login from '../views/Login.vue';
import Trades from '../views/Trades.vue';
import Ativos from '../views/Ativos.vue';
import Carteiras from '../views/Carteiras.vue';
import AtivoDetalhe from '../views/AtivoDetalhe.vue';
import AtivosPosicao from '../views/AtivosPosicao.vue';
import axios from 'axios';
import store from '../store/index.js';

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/trades',
    name: 'Trades',
    component: Trades
  },
  {
    path: '/ativos',
    name: 'Ativos',
    component: Ativos
  },
  {
    path: '/carteiras',
    name: 'Carteiras',
    component: Carteiras
  },
  {
    path: '/ativoDetalhe/:id/:symbol/:nome',
    name: 'AtivoDetalhe',
    component: AtivoDetalhe
  },
  {
    path: '/ativosPosicao',
    name: 'AtivosPosicao',
    component: AtivosPosicao
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, from, next) =>{
  if ( window.$cookies.isKey('user_session') && to.name !== "Login" ){
    const user_session = window.$cookies.get('user_session');
    axios.get(store.state.baseUrl+'/authorize', { params: { "auth_token": user_session} })
    .then((resp) =>{
      resp.status === 200 ? next() : next({ name: 'Login' });
    })
    .catch((error) => {
      window.$cookies.remove('user_session');
      next({ name: 'Login' });
    });
  }else{
    if (to.name !== "Login"){
      next({ name: 'Login' });
    }else{
      next();
    }
  }
});

export default router
